# What?
Bultenoj - Esperanto - Bulletins

A Bullentin board system written in Golang for a friendly little tilde community.

Bultenoj is under development, and not currently usable, but here's a little sneak peak.

### Login
![Bultenoj Login](.imgs/login.png)

### Bulletin Board
![Bultenoj Feed](.imgs/feed.png)

## Building
cd src && go build

## Deps
sqlite3

## Attribution:
Bulletin Board Icon - Edi Prastyo - The Noun Projet
