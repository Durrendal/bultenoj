package main

import (
	"os"
	"log"
	"gopkg.in/yaml.v2"
	
	"bultenoj/views"
	//"bultenoj/data"
)

type Config struct {
	Cache bool `yaml:"Cache"`
	CachePath string `yaml:"CachePath"`
	Port string `yaml:"Port"`
	Remote string `yaml:"Remote"`
	Debug bool `yaml:"Debug"`
}

var cfg Config

func parseConfig(cfg *Config, file string) {
	f, err := os.Open(file)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	parseConfig(&cfg, "config.yaml")
	//data.InitDB(&cfg)

	app := views.Amalgamation()
	if err := app.Run(); err != nil {
		log.Println(err)
	}
}
