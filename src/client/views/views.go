package views

import (
	//"log"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	//"bultenoj/data"
)

/* View Structs */

/* BBS Login UI */
type Login struct {
	Title *tview.TextView //Name
	Form *tview.Form //Login Form
	Footer *tview.TextView //Footer
	*tview.Grid
}

/* BBS Feed UI */
type Feed struct {
	Title *tview.TextView //Name of selected post
	Posts *tview.List //List of Posts
	Feed *tview.TextView //MOTD
	Ctrl *tview.TextView //Controls
	*tview.Grid
}

/* BBS Post UI */
type Post struct {
	Title *tview.TextView //Name of selected post
	Comments *tview.List //List of Comments
	Ctrl *tview.TextView //Controls
	*tview.Grid
}

/* BBS Reply UI */
type PostReply struct {
	Title *tview.TextView //Name of selected post
	Entry *tview.TextView //Text Input Area
	Ctrl *tview.TextView //Controls
	*tview.Grid
}

/* BBS  Settings UI */
type Settings struct {
	Title *tview.TextView //Name of selected post
	Settings *tview.List //Settings list
	Ctrl *tview.TextView //Controls
	*tview.Grid
}

/* View Helpers */
func text(title string) *tview.TextView {
	tv := tview.NewTextView().
		SetTextAlign(tview.AlignLeft).
		SetTextColor(tcell.ColorWhite)

	tv.SetBackgroundColor(tcell.ColorBlack).
		SetBorderColor(tcell.ColorWhite).
		SetBorder(true)

	tv.SetTitle(` ` + title + ` `).
		SetTitleColor(tcell.ColorBlue).
		SetTitleAlign(tview.AlignLeft)
	return tv
}

func list(title string) *tview.List {
	lst := tview.NewList().
		SetMainTextColor(tcell.ColorWhite).
		ShowSecondaryText(false).
		SetShortcutColor(tcell.ColorLightGreen)

	lst.SetBackgroundColor(tcell.ColorBlack).
		SetBorderColor(tcell.ColorWhite).
		SetBorder(true).
		SetTitle(` ` + title + ` `).
		SetTitleColor(tcell.ColorBlue).
		SetTitleAlign(tview.AlignLeft)
	return lst
}

func passprohibit() *tview.Form {
	f := tview.NewForm().
		AddInputField("User: ", "", 24, nil, nil).
		AddPasswordField("Pass: ", "", 24, '*', nil).
		AddButton("Login", nil).
		AddButton("Exit", nil)

	f.SetBorderColor(tcell.ColorWhite).
		SetBorder(true)

	f.SetLabelColor(tcell.ColorLightGreen).
		SetFieldTextColor(tcell.ColorBlack).
		SetFieldBackgroundColor(tcell.ColorDimGray).
		SetButtonTextColor(tcell.ColorWhite).
		SetButtonBackgroundColor(tcell.ColorLightGreen)
	return f
}

/* Login View */
func loginUI() *Login {
	v := &Login{
		Title: text("Bultenoj").SetText(" OK, meetings are virtual ... I don't know!"),
		Form: passprohibit(),
		Footer: text(""),
		Grid: tview.NewGrid(),
		//Model here
	}
	v.Grid.
		//3 is effectively 1 (borders + 1 line of text)
		SetRows(3, 0, 3). //1 row title, ++ rows content between the two, 1 row control
		SetBorders(false). //use view borders
		AddItem(v.Title, 0, 0, 1, 3, 0, 0, false).
		AddItem(v.Form, 1, 0, 1, 3, 0, 0, true).
		AddItem(v.Footer, 2, 0, 1, 3, 0, 0, false)
	//prim, row, col, rowspan, colspan, minH, minW, focus
	return v
}

/* Feed View */
func feedUI() *Feed {
	v := &Feed{
		Title: text("Bulteno"),
		Posts: list("Posts"),
		Feed: text("").SetScrollable(true),
		Ctrl: text("Controls"),
		Grid: tview.NewGrid(),
		//Model here
	}
	v.Grid.
		//3 is effectively 1 (borders + 1 line of text)
		SetRows(3, 0, 3). //1 row title, ++ rows content between the two, 1 row control
		SetBorders(false). //use view borders
		AddItem(v.Title, 0, 0, 1, 3, 0, 0, false).
		AddItem(v.Posts, 1, 0, 1, 1, 0, 0, true).
		AddItem(v.Feed, 1, 1, 1, 2, 0, 0, false).
		AddItem(v.Ctrl, 2, 0, 1, 3, 0, 0, false)
	//prim, row, col, rowspan, colspan, minH, minW, focus
	return v
}

//func Login() {
//	form := tview.NewForm().
//		AddInputField("User: ", "", 24, nil, nil).
//		AddPasswordField("Pass: ", "", 24, '*', nil).
//		AddButton("Login", func() {
//		}).
//		AddButton("Exit", func() {
//			app.Stop()
//		})
//	form.SetBorder(true).SetTitle("Bultenoj Login").SetTitleAlign(tview.AlignLeft)
//	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
//		log.Println(err)
//	}
//}


/* View Amalgamation */
func Amalgamation() *tview.Application {
	app := tview.NewApplication()

	//login(app)
	
	pages := tview.NewPages().
		AddPage("Login", loginUI(), true, true).
		AddPage("Feed", feedUI(), true, false)
	app.SetRoot(pages, true).
		EnableMouse(true).
		SetInputCapture(func (event *tcell.EventKey) *tcell.EventKey {
			switch event.Key() {
			case tcell.KeyCtrlC:
				app.Stop()
			case tcell.KeyCtrlL:
				pages.SwitchToPage("Login")
			case tcell.KeyTAB:
				pages.SwitchToPage("Feed")
			}
			return nil
		})
	return app
}
