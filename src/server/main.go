package main

import (
	"os"
	"log"
	"gopkg.in/yaml.v2"
	
	"bultenoj/data"
)

type Config struct {
	Database string `yaml:"Database"`
	Listen string `yaml:"Listen"`
	Port string `yaml:"Port"`
	Debug bool `yaml:"Debug"`
}

var cfg Config

func parseConfig(cfg *Config, file string) {
	f, err := os.Open(file)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	parseConfig(&cfg, "config.yaml")
	data.InitDB(&cfg)
}
