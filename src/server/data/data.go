package data

import (
	_ "github.com/mattn/go-sqlite3"
	"database/sql"
	"log"
	"os"
)

type Config struct {
	Database string `yaml:"Database"`
	Port int `yaml:"Port"`
	Debug bool `yaml:"Debug"`
}

var cfg Config

/* Table Structs */

//User Info
type User struct {
	Name string
	Pass string
	CreatedOn string
	LastLogin string
}

//MOTD/Announcements
type Announce struct {
	Message string
	Active bool
}

//Post Feed
type Feed struct {
	Title string
	Comments int
}

//Posts
type Post struct {
	Title string
	Comment string
	Author string
	CreatedOn string
}

/* Schemas */
var UserSchema = "CREATE TABLE IF NOT EXISTS users (
id INTEGER PRIMARY KEY,
name string NOT NUL,
password string NOT NULL,
presence DATETIME,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"

var AuthSchema = "CREATE TABLE IF NOT EXISTS auth (
id INTEGER PRIMARY KEY,
key string NOT NULL,
user string NOT NULL,
authorized BOOLEAN DEFAULT true NOT NULL
created_on DATETIME DEFAULT CURRENT_TIMESTAMP
FOREIGN KEY (user) REFERENCES user (id) ON DELETE CASCADE);)"

var AnnounceSchema = "CREATE TABLE IF NOT EXISTS announce (
id INTEGER PRIMARY KEY,
message string,
active boolean,
start DATETIME,
end DATETIME
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"

var PostSchema = "CREATE TABLE IF NOT EXISTS post (
id INTEGER PRIMARY KEY,
title string NOT NULL,
content string NOT NULL,
author string NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"

var CommentSchema = "CREATE TABLE IF NOT EXISTS comment (
id INTEGER PRIMARY KEY,
comment string NOT NULL,
author string NOT NULL,
post string NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (post) REFERENCES feed (id) ON DELETE CASCADE);"

func evalStmt(db *sql.SQLiteConn, schema string) {
	stmt, err := db.Prepare(schema)
	if err != nil {
		log.Fatal(err)
	}
	stmt.Exec()
}

func InitDB(cfg *Config) {
	//If the DB doesn't exist create it
	if _, err := os.Stat(cfg.Database); err != nil {
		os.Create(cfg.Database)
	}

	//Open a connection to the database
	db, err := sql.Open("sqlite3", cfg.Database)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//If db doesn't exist, create it
	rows, _ := db.Query("select 1 from announce;")
	if rows == nil {
		evalStmt(db, UserSchema)
		evalStmt(db, AnnounceSchema)
		evalStmt(db, FeedSchema)
		evalStmt(db, PostSchema)
	}
}

